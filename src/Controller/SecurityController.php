<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordHasherInterface  $hasher,
    EntityManagerInterface $manager): Response
    {
        $request = json_decode($request->getContent());

        $username = $request->username;
        $password = $request->password;

        $user = new User();
        $user->setUsername($username);
        $user->setPassword($hasher->hashPassword($user, $password));

        $manager->persist($user);
        $manager->flush();

        $retour = ["success"=> true];

        return new JsonResponse($retour);
    }
}
